<?php
$ch = curl_init();

curl_setopt($ch, CURLOPT_URL,"http://api.opendevicelab.com/?countries=United%20Kingdom");

// receive server response ...
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

$server_output = curl_exec($ch);

curl_close($ch);

echo $_GET['callback'] . '(' . $server_output . ');';
?>