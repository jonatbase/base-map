var odlmap;
var markerLocations = [];

function loadScript() {
  // Pull in Google Maps API
  var script = document.createElement("script");
  script.type = "text/javascript";
  script.src = "http://maps.googleapis.com/maps/api/js?key=AIzaSyCxPTIi3JvcdpJltPpE9yPuFfCMW80-0ig&sensor=false&callback=getLocations";
  document.body.appendChild(script);
}

function initialize() {
  var mapOptions = {
    zoom: 6,
    center: new google.maps.LatLng(54.019066, -3.186035),
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var odlmap = new google.maps.Map(document.getElementById("odl-map"), mapOptions);
  
  for(var m=0;m<markerLocations.length;m++) {
    var marker = new google.maps.Marker({
      position: new google.maps.LatLng(markerLocations[m].lat, markerLocations[m].lng),
      map: odlmap,
      title: markerLocations[m].title,
      draggable: false,
      animation: google.maps.Animation.DROP
    });
  }

}

function getLocations() {
  $.ajax({
    type: "GET",
    url: "data.php?callback=?",
    dataType: "jsonp",
    success: function(json){
      console.log("SUCCESS");
      $.each(json, function(i, entry){
        console.log("sample:", entry.loc.latlng.lat + ", " + entry.loc.latlng.lng);
        var odl_location = {
          "title": entry.name,
          "lat": entry.loc.latlng.lat,
          "lng": entry.loc.latlng.lng
        }
        markerLocations.push(odl_location);
      });
      initialize();
    },
    error: function(err){
      console.log("ERROR");
      console.log(err);
    }
  });
}

// function PlotMarker(lat, lng){
//   var marker = new google.maps.Marker({
//     position: new google.maps.LatLng(lat, lng),
//     map: map,
//     draggable: false,
//     animation: google.maps.Animation.DROP
//   });
//   markerLocations.push(marker);
// }

window.onload = loadScript;